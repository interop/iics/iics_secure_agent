# syntax = docker/dockerfile:1.0-experimental
FROM ubuntu:22.04

# From https://github.com/jbrazda/ic-sagent-docker

LABEL maintainer="integration-platform@doit.wisc.edu"

# POD and REGION can be inferred from the web URL of your Informatica Cloud site.
# The values for POD and REGION probably need changing for you.  Setting these as
# default for UW-Madison hosted service

ARG POD=usw3
ARG REGION=dm-us
ENV POD=$POD
ENV REGION=$REGION
ENV TZ=America/Chicago

# You should be able to download the Secure Agent binary from the following URL without authentication:

ARG AGENT_URL="https://global-package.dm.informaticacloud.com/package-manager/files/binary/${REGION}_informaticacloud/agent64_install_ng_ext/7232/linux64/agent64_install_ng_ext.7232.bin"
ARG USER=agent
ENV INFA_HOME=/home/$USER/infaagent

# install system tools
RUN apt-get update 
RUN apt-get install -y curl 
RUN apt-get install -y jq
RUN apt-get install -y less 
RUN apt-get install -y libsqliteodbc 
RUN apt-get install -y locales 
RUN apt-get install -y locales-all 
RUN apt-get install -y sqlite3 
RUN apt-get install -y sudo 
RUN apt-get install -y tzdata 
RUN apt-get install -y unixodbc 
RUN apt-get install -y unzip 
RUN apt-get install -y wget 
RUN apt-get install -y libidn12 
RUN apt-get install -y python3

# UBUNTU is compatible with libidn12, but secure agent looks for a libidn11. We need to create a symbolic link. 
RUN sudo chmod 744 /usr/lib/x86_64-linux-gnu/libidn.so.12 &&\
  ln -s /usr/lib/x86_64-linux-gnu/libidn.so.12 /usr/lib/x86_64-linux-gnu/libidn.so.11

# set 'python' command to refer to python3
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

RUN ln -fs /usr/share/zoneinfo/$TZ /etc/localtime \
&& dpkg-reconfigure -f noninteractive tzdata

# Snowflake ODBC
RUN wget --no-directories --recursive --level=1 --no-parent --accept 'snowflake-odbc*.deb' https://sfc-repo.snowflakecomputing.com/odbc/linux/latest/index.html && \
dpkg -i snowflake-odbc*.deb && \
rm snowflake-odbc*.deb

# MySQL ODBC https://docs.informatica.com/integration-cloud/cloud-data-integration-connectors/current-version/mysql-connector/introduction-to-mysql-connector/administration-of-mysql-connector/installing-mysql-odbc-driver/installing-mysql-odbc-driver-on-linux.html
COPY drivers/mysql/odbc/* /usr/lib/x86_64-linux-gnu/odbc/

# Set the locale, Locale defaults are necessary for agent to operate correctly
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# We need to run docker image under a different user than root
# Secure agent process engine can't be run under root account
RUN useradd --create-home -ms /bin/bash -U $USER
RUN mkdir /exports && chown $USER:$USER /exports
USER $USER

# Add the SQLite and MySQL ODBC information. The main odbcinst.ini is in INFA_HOME directory.
ADD odbcinst.ini /home/agent/.odbcinst.ini

# Create directories so that volume mounts pick up the correct users
RUN mkdir -p ~/databases && \
  mkdir -p ~/data && \
  mkdir -p $INFA_HOME/apps/Administrator/logs && \
  mkdir -p $INFA_HOME/apps/agentcore/logs && \
  mkdir -p $INFA_HOME/apps/CIHProcessor/logs && \
  mkdir -p $INFA_HOME/apps/Common_Integration_Components/logs && \
  mkdir -p $INFA_HOME/apps/Data_Integration_Server/logs && \
  mkdir -p $INFA_HOME/apps/FileIntegrationService/logs && \
  mkdir -p $INFA_HOME/apps/MassIngestionRuntime/logs && \
  mkdir -p $INFA_HOME/apps/OpsInsightsDataCollector/logs && \
  mkdir -p $INFA_HOME/apps/process-engine/logs && \
  mkdir -p $INFA_HOME/apps/Data_Integration_Server/ext/drivers && \
  mkdir -p $INFA_HOME/apps/agentcore/conf && \
  mkdir -p $INFA_HOME/apps/process-engine/ext && \
  mkdir -p /home/agent/.postgresql

# Install MySQL driver required by MySQL connector for Cloud Data Integration. https://docs.informatica.com/integration-cloud/cloud-data-integration-connectors/current-version/mysql-connector/introduction-to-mysql-connector/administration-of-mysql-connector/installing-mysql-jdbc-driver.html
COPY drivers/mysql/jdbc/mysql-connector-j-9.1.0.jar $INFA_HOME/apps/Data_Integration_Server/ext/drivers

# Install Oracle JDBC driver required by JDBC connector for Cloud Application Integration. https://docs.informatica.com/integration-cloud/cloud-application-integration/current-version/jdbc-connector-guide/introduction-to-jdbc-connector/administration-of-jdbc-connector.html
COPY drivers/ojdbc11.jar $INFA_HOME/apps/process-engine/ext

# Install PostgreSQL JDBC driver required by JDBC connector for Cloud Application Integration. https://docs.informatica.com/integration-cloud/cloud-application-integration/current-version/jdbc-connector-guide/introduction-to-jdbc-connector/administration-of-jdbc-connector.html
RUN wget https://jdbc.postgresql.org/download/postgresql-42.6.0.jar -O $INFA_HOME/apps/process-engine/ext/postgresql.jar

# Download the AWS RDS SSL root certificate file.
RUN wget https://truststore.pki.rds.amazonaws.com/global/global-bundle.pem -O /home/agent/.postgresql/root.crt

# Give read access TO the root certificate file.
RUN chmod 644 /home/agent/.postgresql/root.crt

  # Create placeholder executables for Tableau connector so that they have the correct permissions.
# The Tableau connector requires these executables, but it does not have the permissions to execute them, by default.
# https://network.informatica.com/onlinehelp/IICS/prod/CDI/en/index.htm#page/cloud-data-integration-tableau-v3-connector/Administration_of_Tableau_V3_Connector.html
ENV TABLEAU_EXECUTABLES_DIR=downloads/package-TableauV3.7/package/tableauv3/libs/hyper
RUN mkdir -p $INFA_HOME/$TABLEAU_EXECUTABLES_DIR && \
  touch $INFA_HOME/$TABLEAU_EXECUTABLES_DIR/hyperd $INFA_HOME/$TABLEAU_EXECUTABLES_DIR/hyperd_sse2 $INFA_HOME/$TABLEAU_EXECUTABLES_DIR/hyperdstarter && \
  chmod 744 $INFA_HOME/$TABLEAU_EXECUTABLES_DIR/*

# 1. Download and prepare Installer
# 2. Set file permissions
# 3. Install using silent install and the default location
# 4. Cleanup
RUN curl -o /tmp/agent64_install.bin $AGENT_URL && \
chmod +x /tmp/agent64_install.bin && \
/tmp/agent64_install.bin -i silent && \
rm -rf /tmp/agent64_install.bin

WORKDIR $INFA_HOME/apps/agentcore

COPY run_agent.sh .

CMD [ "./run_agent.sh" ]
