## Table Of Contents
* [User Guide](#user-guide)
* [IICS Secure Agent Docker Image](#iics-secure-agent-docker-image)
    * [Description](#description)
    * [Requirements](#requirements)
    * [The Image](#the-image)
        * [ODBC](#odbc)
        * [Image Scanning](#image-scanning)
    * [Containers](#containers)
        * [Environment Variables](#environment-variables)
        * [Externalized Configurations](#externalized-configurations)
        * [Starting](#starting)
        * [Monitoring](#monitoring)
    * [Hardware Requirements](#hardware-requirements)
    * [Known Issues](#known-issues)
    * [Enterprise Integration Secure Agent](#enterprise-integration-secure-agent)
        * [Architecture Diagram](#architecture-diagram)
        * [CI/CD and Terraform](#cicd-and-terraform)
        * [Informatica User](#informatica-user)
        * [Credentials in Terraform](#credentials-in-terraform)
            * [Test Environment](#test-environment)
            * [Production Environment](#production-environment)
        * [Memory and CPU for Secure Agent](#memory-and-cpu-for-secure-agent)
        * [EFS Mounting, Accessing Secure Agent Configurations, Logs and Additional Debugging](#efs-mounting-accessing-secure-agent-configurations-logs-and-additional-debugging)

# User Guide
[userguide]: #user-guide
User guide for AWS hosted Secure Agent is available [here](./userguide.md).

# IICS Secure Agent Docker Image
[dockerimage]: #iics-secure-agent-docker-image
This document covers following topics:
1. How ro run IICS Secure Agent using Docker image.
2. IICS Secure Agent deployed(including Terraform configuration) in AWS(interop).

## Description
[description]: #description
This project was forked from [jbrazda/ic-sagent-docker](https://github.com/jbrazda/ic-sagent-docker) with the intention of containerizing the IICS Secure Agent to run in Amazon Web Services.  As of early January 2019, there is no official Docker image for IICS Secure Agent.

## Requirements
[requirement]: #requirements
* Docker.
* An IICS user account and password for your organization that has appropriate rights to create and manage secure agents. This user will be used to login to the Informatica APIs to register the agent.
At a minimum, the user should be assigned a role that has Create, Read, and Update privileges for the "Secure Agent" and "Secure Agent Group" asset types.
* Your Informatica POD and REGION, which can be inferred from https://${POD}.${REGION}.informaticacloud.com/.  Defaults are set to POD=usw3 and REGION=dm-us, but can be overriden with Docker environment variables.

## The Image
[image]: #the-image
The image is based on Ubuntu 18.04.  It downloads and installs necessary dependencies, and then downloads the installer file from the correct Informatica URL based on your POD and REGION.

1. Clone this repository.
2. `docker build . -t iics_secure_agent:<tag>` - optionally pass --build-arg parameters for POD and REGION if the defaults are not correct.  POD and REGION are also set as environment variables, because containers will use these values for communicating with the Informatica API.

### ODBC
[odbc]: #odbc
ODBC support has been added to this image, specifically support for SQLite3 and Snowflake.  An odbc.ini file can be mounted into the container as /home/agent/.odbc.ini for setting up connections.  Local files can be mounted or copied into the container as necessary.

#### Debugging ODBC Connections

* Check file permissions on ODBC.ini, which needs read/write by the user.  You will not get an error if the file permissions are incorrect, but the sources will not appear.
* Use `odbcinst` to show configuration, data sources, and drivers by opening a shell in the docker container using `docker exec -ti container_name bash`.

```bash
# Show the configuration.  This does not list .odbc.ini and .odbcinst.ini in the home directory.
agent@odmasprod:~/infaagent/apps/agentcore$ odbcinst -j
unixODBC 2.3.4
DRIVERS............: /etc/odbcinst.ini
SYSTEM DATA SOURCES: /etc/odbc.ini
FILE DATA SOURCES..: /etc/ODBCDataSources
USER DATA SOURCES..: /home/agent/.odbc.ini
SQLULEN Size.......: 8
SQLLEN Size........: 8
SQLSETPOSIROW Size.: 8
```

```bash
# Show installed drivers.  Verify that the correct drivers are found.
agent@odmasprod:~/infaagent/apps/agentcore$ odbcinst -q -d
[SQLite]
[SQLite3]
[SnowflakeDSIIDriver]
```

```bash
# Show data sources.  These have been mounted into the container from the odbc.ini file.
agent@odmasprod:~/infaagent/apps/agentcore$ odbcinst -q -s
[ODBC_database_1]
[ODBC_database_2]
[chinook]
```

### Image Scanning
[imagescan]: #image-scanning
The image is scanned by [Qualys](https://qualysguard.qg2.apps.qualys.com/cs/help/get_started/get_started.htm), a container image vulnerability
scanning system, through a Gitlab CI/CD job: `image_scan`. This job is built upon the `qualy_scan` job from the`check-qualys-job.yml` template in [checkQualys](https://git.doit.wisc.edu/interop/checkqualys) tool repository. An `image_scan` job will be created and a [Gitlab issue](https://git.doit.wisc.edu/interop/iics/iics_secure_agent/-/issues) will be added to this repository for each [CVE vulnerability](https://www.cvedetails.com/) that occurs.

To enable container image scanning, you need to first check following criteria:

* A Gitlab API Project Access Token for checkQualys tool is valid and stored as `CI_TOKEN` in **Settings -> CI/CD -> Environemnt Variables**, if not, you need to generate one in checkQualys repository under **Settings -> Access Token**, following the instruction [here](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html);
* A Gitlab runner in **Settings -> CI/CD -> Runners** with **qualys** tag is enabled.

Notes:

* A Gitlab API Project Access Token specify for checkQualys is necessary and can be found in checkQualys repository that allows the checkQualys tool to post issues back to the project that is being scanned.
* A manual `qualys_scan` job at post stage will be created when including job template from checkQualys, and cannot be removed, but it can be ignored.
* This scan job will report all vulnerabilities in the Gitlab issue once occur, if there are a particular vulnerability that does not impact this deployment, a `.cveignore` file with certain CVE ID can be added at the root level to ignore/silence it.

## Containers
[container]: #containers
The container executes a bash script called run_agent.sh upon start.  It needs INFORMATICA_USER and INFORMATICA_PASSWORD environment variables set during runtime in order to communicate with the Informatica API to check and register the Secure Agent.

### Environment Variables
[variables]: #environment-variables
* INFORMATICA_USER (required) - User that can run the agent and access Informatica APIs
* INFORMATICA_PASSWORD (required) - Password for above credential.  Used for API access only.
* JSON_LOG (optional) - If this is set, then the log output will be in JSON format.

**It is best practice to put these environment variables in a Docker [.env](https://docs.docker.com/compose/env-file/) file.**


### Persisting Secure Agent Configuration With Docker Volumes
[configs]: #externalized-configurations
Once the container starts and registers with Informatica API using the INFORMATICA_USER and INFORMATICA_PASSWORD credentials, a file will be created in the INFA_HOME/apps/agentcore/conf directory called `infaagent.ini`.  This file contains the organization and agent ids and the registration token and should be persisted, so that when a container is restarted or redeployed, the configuration will be saved.

In addition to infagent.ini, there are additional files in the INFA_HOME/apps/agentcore/conf directory that you may want to persist, including `proxy.ini` used when running behind a [proxy](https://docs.informatica.com/integration-cloud/cloud-platform/current-version/administrator/runtime-environments/secure-agents/configuring-a-proxy-to-exclude-non-proxy-hosts.html) server.

To persist these configuration files, use Docker [volumes](https://docs.docker.com/storage/volumes/) to either mount individual files or the conf directory into the Docker container.

### Logs and Log Directories

The Secure Agent parent process has a startup log in INFA_AGENT/apps/agentcore.log and port and Tomcat logs in INFA_AGENT/apps/agentcore/logs.

The following directories contain log files for each of the IICS applications:
* INFA_HOME/apps/Administrator/logs
* INFA_HOME/apps/CIHProcessor/logs
* INFA_HOME/apps/Common_Integration_Components/logs
* INFA_HOME/apps/Data_Integration_Server/logs
* INFA_HOME/apps/FileIntegrationService/logs
* INFA_HOME/apps/MassIngestionRuntime/logs
* INFA_HOME/apps/OpsInsightsDataCollector/logs
* INFA_HOME/apps/process-engine/logs

The above file and directories can also be persisted using Docker volumes.

### Ports
There are several ports in use by the secure agent, although it isn't strictly necessary to allow access to these ports in most cases.

  * `7005` - Tomcat Shutdown Port
  * `7080` and `7443` - Process Engine HTTP and HTTPS ports used to call the [Secure Agent as a Platform](https://kb.informatica.com/howto/6/pages/19/507918.aspx) (AAP) directly.  In other words, Application Integration Processes that you have created can be called on a specific Secure Agent by URL, e.g. https://your_agent_IP:7443/process-engine/public/rt/LookupInvoiceAndDetails?invoiceID=123.
* `5432` - Process Engine PostgreSQL DB port.  Used for [load balanced Secure Agent groups](https://network.informatica.com/servlet/JiveServlet/download/17437-2-47463/Cloud+Application+Integration+-+Process+Server+Load+Balancing+and+Clustering+on+Secure+Agent.pdf) that must all connect to the same master database.

See below section for examples on how to use volume and port mapping.

### Starting
[starting]: #starting
* Setting the hostname will provide the associated name in the IICS website.
* Note: since anybody who has access to see the processes can view the values `INFORMATICA_USER` and `INFORMATICA_PASSWORD`, it's recommenced to configure them in a Docker [.env](https://docs.docker.com/compose/env-file/) file.

#### Starting with docker-compose 

Edit the .env file with the following entries.

Required:
```shell
INFORMATICA_USER=xxxx
INFORMATICA_PASSWORD=xxxx
HOSTNAME=xxxx
```

Optional (for volumes):
```shell
CONFIG_DIR=
LOG_DIR=
ODBC_DATABASE_DIR=
APPS_DIR=/home/agent/infaagent/apps
```

Example for HOSTNAME: bob-secure-agent

The included docker-compose.yml file uses the above environment variables to map directories into the container to persist log directories and infaagent.ini and odbc.ini files.  Modify for your use.

For volumes, add volume folders in directory.
For no volumes, delete lines 15 - 32 on docker-compose.yml

```shell
$ docker-compose build
$ docker-compose up
```

#### Starting with docker run

```shell
$ touch agentcore.log infaagent.log
$ docker run -d \
    -h <hostname> \
    --env INFORMATICA_USER=xxxxx \
    --env INFORMATICA_PASSWORD=xxxx \
    -v $(pwd)/infaagent.log:/home/agent/infaagent/apps/agentcore/infaagent.log  \
    -v $(pwd)/agentcore.log:/home/agent/infaagent/apps/agentcore/agentcore.log  \
    -v $(pwd)/logs:/home/agent/infaagent/apps/agentcore/logs  \
    -p 7080:7080 \
    -p 7443:7443 \
    -p 5432:5432 \
    --name <container_name> iics_secure_agent:<tag>
```

* Note that if `conf` directory needs mounting, a minimum configuration for starting the Secure Agent is required, see [conf](./conf) for example.
```shell
$ touch agentcore.log infaagent.log
$ docker run -d \
    -h <hostname> \
    --env INFORMATICA_USER=xxxxx \
    --env INFORMATICA_PASSWORD=xxxx \
    -v $(pwd)/infaagent.log:/home/agent/infaagent/apps/agentcore/infaagent.log  \
    -v $(pwd)/agentcore.log:/home/agent/infaagent/apps/agentcore/agentcore.log  \
    -v $(pwd)/logs:/home/agent/infaagent/apps/agentcore/logs  \
    -v $(pwd)/data:/home/agent/infaagent/apps/agentcore/data \
    -v $(pwd)/conf:/home/agent/infaagent/apps/agentcore/conf \
    -p 7080:7080 \
    -p 7443:7443 \
    -p 5432:5432 \
    --name <container_name> iics_secure_agent:<tag>
```

### Monitoring
[monitor]: #monitoring
If volume mapping is not used use `docker exec` to attach to the running container.

```shell
docker exec -it <container_name> less agentcore.log
```
## Hardware Requirements
[hardware]: #hardware-requirements
Minimum Hardware Specs ([reference](https://network.informatica.com/servlet/JiveServlet/download/189174-25674/Informatica%20Cloud%20Secure%20Agent%20Minimum%20Requirements.pdf)):
* Windows version 7 (32-bit or 64-bit) or Windows XP Pro SP2/SP3 (32-bit).
* Linux (32-bit or 64-bit).
* Red Hat Enterprise Linux version 5.3 or above.
* SUSE Linux Enterprise version 10 and above.
* 2-4 CPU recommended.
* 2 GB RAM recommended dedicated for the Agent (Recent customers have used 8GB).
* 1 GB Hard disk space.
* 1 GB Network connectivity (faster the better as this will most likely the bottleneck).

## Known Issues
[issues]: #known-issues
* Doesn't seem to be able to run in host network mode because it won't be able to talk to internal ports.  It would probably work if you expose those ports.

## Enterprise Integration Secure Agent
[eisecureagnet]: #enterprise-integration-secure-agent
Secure Agent containers are deployed in following tiers in interop/AWS account. This deployment is automated through Terraform (see below section on Terraform).

| environment| tier        |runtime name(default) in IICS |
| -----------|-------------|-------------------------|
| test      | us-east-1(N. Virginia)|ei.secureagent.doit.wisc.edu |
| prod      | us-east-2(Ohio)|ei.secureagent.doit.wisc.edu|

### Architecture Diagram
[diagram]: #architecture-diagram
![Secure Agent](./IICS-secure-agent-deployment.svg)

The diagram source can be found [here](https://www.lucidchart.com/documents/edit/86359940-c63f-492c-8dd5-606b90525b92/0_0?beaconFlowId=C00A2449A556180D).

### CI/CD and Terraform
[terraform]: #cicd-and-terraform
* Infrastructure is available as code in terraform for `AWS` provider, and [.gitlab-ci.yml](.gitlab-ci.yml) pipeline push the Secure Agent
image into [AWS ECR](https://aws.amazon.com/ecr/).
* Before disposing and create a new instance of Informatica Secure Agent, existing live connections to various targets(for e.g. databases)
should be reviewed and terminated. Due to this reason this is not part of CI/CD pipeline and can be executed as per necessity. Also, back up any logs and configurations
before next run using Terraform, see below section on EFS mounting.
* Various configurations can be overridden using  `-var=`, see [variables.tf](./terraform/variables.tf) for available parameters.
See Terraform doc on [variables](https://www.terraform.io/docs/configuration/variables.html) to see how to pass command line arguments.
* Secure Agent Docker images are tagged using the value, `git-${CI_COMMIT_SHA:0:8}` and push to ECR registry in `us-east-1` in CD pipeline ([.gitlab-ci.yml](.gitlab-ci.yml)). The same
image can be used for production set up.
* Note that if this Terraform set up need to use with a different AWS account then the image will have to upload to [AWS ECR](https://aws.amazon.com/ecr/) under that account(so that
can be passed as a Terraform variable for `image_name`, see below for an example).
* Once the Agent is deployed it can be seen under `Administrator`->`Runtime Environments` in IICS(the group name can be configured
in `infaagent.ini` using `InfaAgent.GroupName=aws-interop`).
* Terraform state files are stored in shared S3 buckets `test-interop-terraform-state` and `prod-interop-terraform-state`.

### Informatica User
[user]: #informatica-user
Credentials for Informatica User (for test and prod instances) are stored in parameter store (in regions `us-east-1` and
`us-east-2` respectively) and expected to be available with the following names:
* username - `/iics/cicd_username`.
* password - `/iics/cicd_password`.

### Credentials in Terraform
[cred]: #credentials-in-terraform
* It's recommended to define all variables values in a `*.tfvars` file and pass that to terraform using `-var-file` argument.
 This will avoid having any sensitive parameters in bash history.

```shell script
$ cd terraform
```
#### Test Environment
[testenv]: #test-environment

If your aws profile differs from "default", override it using the `aws_profile` variable.

```shell script
$ terraform init -backend-config="bucket=test-interop-terraform-state" \
                 -backend-config="key=ei.secureagent.tfstate" \
                 -backend-config="region=us-east-1"
$ terraform apply -var-file=test.secure_agent.tfvars -var="aws_profile=<your_aws_profile_name>"
```
#### Production Environment
[prodenv]: #production-environment
```shell script
$ terraform init -backend-config="bucket=prod-interop-terraform-state" \
                 -backend-config="key=ei.secureagent.tfstate" \
                 -backend-config="region=us-east-2"
$ terraform apply -var-file=prod.secure_agent.tfvars -var="aws_profile=<your_aws_profile_name>"
```

### Memory and CPU for Secure Agent
[memory]: #memory-and-cpu-for-secure-agent
* By default, Secure Agent is deployed on to a `t2.xlarge` instance and container is given an 15GB of memory. `instance_type`
and `container_memory` variables can be used to adjust these values. See [variables.tf](./terraform/variables.tf) for details.

### EFS Mounting, Accessing Secure Agent Configurations, Logs and Additional Debugging
[debug]: #efs-mounting-accessing-secure-agent-configurations-logs-and-additional-debugging
* Above terraform configuration creates a EFS file system that is used by the container to persist Secure Agent configurations
and logs files.
* One way to access Secure Agent's log files and configurations files is to mount the EFS file system use by Secure Agent.
EFS file system is accessible from any instance within `test` and `prod` tiers. Start an EC2 instance in `test-tier` VPC
(or in prod VPC - `prod-tier `) and this instance can be used to mount the EFS file system (be sure to that this EC2 instance can be accessed from SSH).
Then use the instructions documented in `Settings` in Secure Agent's EFS file system(`iics-secure-agent`) under `File system access` in AWS console.
* If there's a requirement to log into EC2 instance that runs the container, [AWS Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html) can be [used](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-quick-setup.html).
