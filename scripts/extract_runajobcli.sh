#!/bin/bash

# Script to pull the needed files for runAJobCli from the Secure Agent container
# to the local directory.  Should probably be run on a regular basis in order
# to update the libraries.  Was developed for INPLATFORM-383

# After this is run, still need to copy restenv_default and log4j_default files
# to the regular files, create the logs directory, and set permissions.

# What container should this run on?  It is probably safe to hard code this, but
# an alternative would be to do a docker ps and pull the correct container
container_name=

# Directory in the container where runAJobCli lives.
run_a_job_cli_home=/home/agent/infaagent/apps/runAJobCli

docker cp -L $container_name:$run_a_job_cli_home/cli.sh .
docker cp -L $container_name:$run_a_job_cli_home/runAJobCli.jar .
docker cp -L $container_name:$run_a_job_cli_home/restenv_default.properties .
docker cp -L $container_name:$run_a_job_cli_home/log4j_default.properties .

mkdir -p lib

for i in $(docker exec $container_name ls $run_a_job_cli_home/lib/)
do
	docker cp -L $container_name:$run_a_job_cli_home/lib/$i lib
done

chomd +x cli.sh
