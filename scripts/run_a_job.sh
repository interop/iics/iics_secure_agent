#!/bin/bash

# This script will run the runAJobCli inside the docker container.  The user that
# runs this script needs to be in the docker group or it needs to be run with sudo.
# Was developed for INPLATFORM-383

# What container should this run on?  It is probably safe to hard code this, but
# an alternative would be to do a docker ps and pull the correct container
container_name=

# Directory in the container where runAJobCli lives.
run_a_job_cli_home=/home/agent/infaagent/apps/runAJobCli

# This is a little hard to read.  Change to the directory and see if restenv.properties exists.
# If it does not exist, then cat the template file, set the variables with values from the
# environment variables, which have already been set to start the agent, and then redirect
# output to the correct file.  Run this each time the command is run in case this is a new container.

restenv_command="cd $run_a_job_cli_home "\
"&& [[ ! -e restenv.properties ]] "\
"&& cat restenv_default.properties "\
"| sed \"s/username=username/username=\$INFORMATICA_USER/\""\
"| sed \"s/password=password/password=\$INFORMATICA_PASSWORD/\""\
" > restenv.properties"

docker exec -ti $container_name bash -c "${restenv_command}"

# Same thing for the log4j_properties file.  The default is OK for this file.
docker exec -ti $container_name bash -c "cd $run_a_job_cli_home && [[ ! -e log4j.properties ]] && cp log4j_default.properties log4j.properties"

# This does not seem to be set to execute privilege by default.  :(
docker exec -ti $container_name bash -c "cd $run_a_job_cli_home && [[ -x cli.sh ]] && chmod +x cli.sh"

# Quote the arguments to this script to pass to the docker container
# Thanks to https://superuser.com/questions/403263/how-to-pass-bash-script-arguments-to-a-subshell
quoted_args="$(printf "${1+ %q}" "$@")"

# Run the cli.sh script in the container.
docker exec -ti $container_name bash -c "cd $run_a_job_cli_home && PATH=$PATH:/home/agent/infaagent/jdk/bin ./cli.sh runAJobCli $quoted_args"
