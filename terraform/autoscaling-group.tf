data "aws_ssm_parameter" "ecs-optimized" {
  name = "/aws/service/ecs/optimized-ami/amazon-linux-2/recommended/image_id"
}

resource "aws_launch_configuration" "secure-agent-launch-config" {
  # Fix me: Make this use the ecs-optimized parameter so that it always uses the latest version.
  image_id = var.ecs_optimized_ami

  enable_monitoring = false

  iam_instance_profile = aws_iam_instance_profile.ecs-instance-profile.name
  security_groups      = [data.aws_security_group.sec-group.id, data.aws_security_group.default.id]
  user_data            = <<EOF
              #!/bin/bash
              echo ECS_CLUSTER=${var.ecs_cluster_name} >> /etc/ecs/ecs.config
              EOF
  instance_type        = var.instance_type
  root_block_device {
    volume_size = var.instance_ebs_size
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "secure-agent-autoscaling-group" {
  # as per our current licesning in IICS, each docker instance that
  # runs on EC2 will treat at as a new license.
  name             = "secure-agent-autoscaling-group-${aws_launch_configuration.secure-agent-launch-config.name}"
  desired_capacity = 1
  max_size         = 1
  min_size         = 1
  min_elb_capacity = 1

  # make sure deployed EC2 instance is in the same AZ as the EFS
  # see https://docs.aws.amazon.com/efs/latest/ug/mounting-fs-mount-cmd-dns-name.html
  vpc_zone_identifier  = [sort(data.aws_subnets.subnets.ids)[0]]
  health_check_type    = "EC2"
  launch_configuration = aws_launch_configuration.secure-agent-launch-config.name
  tag {
    key                 = "Name"
    value               = "IICS Secure Agent"
    propagate_at_launch = true
  }
  lifecycle {
    create_before_destroy = true
  }
}
