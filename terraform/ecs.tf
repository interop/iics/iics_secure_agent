data "aws_ssm_parameter" "informatica-username" {
  name = "/iics/cicd_username"
}

data "aws_ssm_parameter" "informatica-password" {
  name = "/iics/cicd_password"
}

resource "aws_iam_role" "ecs-task-execution" {
  name               = var.ecs_execution_role
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect":"Allow"
    }
  ]
}
EOF
  tags = {
    Name = "iics-ecs-execution-role"
  }
}

# grant role permission for ECS task execution
resource "aws_iam_role_policy_attachment" "ecs-task-execution" {
  role       = aws_iam_role.ecs-task-execution.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

# grant access for SSM for credentails look up
resource "aws_iam_policy" "iics-ssm-policy" {
  name   = var.iics_secret_access_policy
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ssm:GetParameters"
      ],
      "Resource": [
          "${data.aws_ssm_parameter.informatica-username.arn}",
          "${data.aws_ssm_parameter.informatica-password.arn}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "credentails" {
  role       = aws_iam_role.ecs-task-execution.name
  policy_arn = aws_iam_policy.iics-ssm-policy.arn
}

resource "aws_ecs_task_definition" "task" {
  family             = var.ecs_task_name
  execution_role_arn = aws_iam_role.ecs-task-execution.arn
  network_mode       = var.container_network_mode
  requires_compatibilities = [
  "EC2"]
  container_definitions = templatefile("./templates/container-definitions.tpl",
    {
      container_name          = var.container_name
      image_name              = var.image_name
      container_memory        = var.container_memory
      container_hostname      = var.container_hostname
      app_port1               = var.container_app_port[0]
      app_port2               = var.container_app_port[1]
      app_port3               = var.container_app_port[2]
      informatica_username    = data.aws_ssm_parameter.informatica-username.arn
      informatica_password    = data.aws_ssm_parameter.informatica-password.arn
      secure_agent_mount_path = var.secure_agent_mount_path
  })
  volume {
    name = "wiscalerts"
    docker_volume_configuration {
      autoprovision = true
      scope         = "shared"
      driver        = "local"

      driver_opts = {
        "type"   = "nfs"
        "device" = "${aws_efs_file_system.secure-agent-fs.dns_name}:/wiscalerts"
        "o"      = "addr=${aws_efs_file_system.secure-agent-fs.dns_name},nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,nosuid"
      }
    }
  }

  volume {
    name = "partner-integrations"
    docker_volume_configuration {
      autoprovision = true
      scope         = "shared"
      driver        = "local"

      driver_opts = {
        "type"   = "nfs"
        "device" = "${aws_efs_file_system.secure-agent-fs.dns_name}:/partner-integrations"
        "o"      = "addr=${aws_efs_file_system.secure-agent-fs.dns_name},nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,nosuid"
      }
    }
  }
  tags = var.ecs_task_tags
}

resource "aws_ecs_service" "service" {
  name            = var.ecs_service_name
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.task.arn

  # Ensures that only one secure agent is running at a given time. Otherwise there may not be enough memory available on the instance.
  desired_count                      = 1
  deployment_minimum_healthy_percent = 0
  deployment_maximum_percent         = 100

  # secure agent configs and logs are persisted into an EFS volume.
  launch_type = "EC2"
}

resource "aws_ecs_cluster" "cluster" {
  name = var.ecs_cluster_name
  tags = var.ecs_cluster_tags
}
