resource "aws_efs_file_system" "secure-agent-fs" {
  tags = var.efs_tags
}

output "aws_efs_token" {
  value = aws_efs_file_system.secure-agent-fs.creation_token
}

resource "aws_efs_mount_target" "secure-agent-fs-mount" {
  file_system_id = aws_efs_file_system.secure-agent-fs.id
  subnet_id      = sort(data.aws_subnets.subnets.ids)[0]
  security_groups = [
  data.aws_security_group.sec-group.id]
}

resource "aws_efs_backup_policy" "policy" {
  file_system_id = aws_efs_file_system.secure-agent-fs.id
  backup_policy {
    status = "ENABLED"
  }
}