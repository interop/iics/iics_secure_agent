data "aws_vpc" "vpc" {
  tags = var.vpc_tags
}

data "aws_subnets" "subnets" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }
  filter {
    name = "tag:Name"
    values = [
    var.private_subnets_filter["Name"]]
  }
}
