data "aws_security_group" "sec-group" {
  vpc_id = data.aws_vpc.vpc.id
  tags   = var.security_group
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.vpc.id
  name   = "default"
}

// open port 2049 for NFSv4
resource "aws_security_group" "secure-agent-efs-sg" {
  name   = var.secure_agent_efs_sg
  vpc_id = data.aws_vpc.vpc.id

  // NFS
  ingress {
    security_groups = [
    data.aws_security_group.sec-group.id]
    from_port = 2049
    to_port   = 2049
    protocol  = "tcp"
  }

  # allow SSH connections from configured security group,
  # for debugging purposes(can set up a bastion host).
  ingress {
    security_groups = [
    data.aws_security_group.sec-group.id]
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
  }

  egress {
    security_groups = [
    data.aws_security_group.sec-group.id]
    from_port = 0
    to_port   = 0
    protocol  = "-1"
  }

  tags = var.secure_agent_sg_tags
}