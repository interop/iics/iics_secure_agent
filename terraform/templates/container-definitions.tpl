[
  {
    "name": "${container_name}",
    "image": "${image_name}",
    "memory": ${container_memory},
    "hostname": "${container_hostname}",
    "portMappings": [
      {
        "containerPort": ${app_port1},
        "hostPort": ${app_port1}
      },
      {
        "containerPort": ${app_port2},
        "hostPort": ${app_port2}
      },
      {
        "containerPort": ${app_port3},
        "hostPort": ${app_port3}
      }
    ],
    "mountPoints": [
        {
            "containerPath": "${secure_agent_mount_path}wiscalerts/",
            "sourceVolume": "wiscalerts"
        },
        {
            "containerPath": "${secure_agent_mount_path}partner-integrations/",
            "sourceVolume": "partner-integrations"
        }
    ],
    "secrets":[
        {
            "name":"INFORMATICA_USER",
            "valueFrom":"${informatica_username}"
        },
        {
            "name":"INFORMATICA_PASSWORD",
            "valueFrom":"${informatica_password}"
        }
    ],
    "systemControls":[
        {
            "namespace":"net.ipv4.tcp_keepalive_intvl",
            "value":"10"
        },
        {
            "namespace":"net.ipv4.tcp_keepalive_probes",
            "value":"3"
        },
        {
            "namespace":"net.ipv4.tcp_keepalive_time",
            "value":"60"
        }
    ]
  }
]
