variable "aws_profile" {
  default = "default"
}

variable "aws_account_id" {
  type = list(string)
  default = [
    "265723766240"
  ]
}

variable "aws_region" {
  # test tier
  default = "us-east-1"
}

variable "vpc_tags" {
  type = map(string)
  default = {
    Name = "test-tier"
    tier = "test"
  }
}

# needs at least two subnets
variable "private_subnets_filter" {
  type = map(string)
  default = {
    Name = "test-private-*"
  }
}

variable "security_group" {
  type = map(string)
  default = {
    tier = "test"
    Name = "internal"
  }
}

# see git tagging in .gitlab-ci.yml for image name.
variable "image_name" {
  description = "Secure Agent image name publish by .gitlab-ci.yml."
}

variable "container_name" {
  description = "Container name for Secure Agent image."
  default     = "iics-secure-agent-test"
}
variable "container_network_mode" {
  # in order to pass a container hostname bridge network mode is used
  # instead of awsvpc mode.
  default = "bridge"
}

variable "container_hostname" {
  default = "ei.secureagent.doit.wisc.edu"
}

variable "container_memory" {
  default = 31744 # 31GiB
}

variable "instance_ebs_size" {
  default = 250 # 250GB
}

# see Secure Agent system requirements, before changing instance type, see the
# supported instance types for launch configuration. See `container_memory` above for
# configuring memory for container process.
variable "instance_type" {
  default = "t2.2xlarge" # 8 vCPU, 32 GB
}

variable "container_app_port" {
  type = list(string)
  default = [
    7080,
    7443,
  5432]
}

# note that as per our licesning model each
# container would be an extran instance
variable "container_count" {
  default = 1
}

variable "ecs_iam_role" {
  default = "iics-secure-agent-iam-role-test"
}

variable "ecs_execution_role" {
  default = "iics-secure-agent-ecs-execution-role-test"
}

variable "ecs_cluster_name" {
  default = "iics-agent-cluster"
}

variable "ecs_cluster_tags" {
  type = map(string)
  default = {
    Name = "iics-agent-cluster"
    tier = "test"
  }
}

variable "ecs_task_name" {
  default = "iics-agent-task"
}

variable "ecs_task_tags" {
  type = map(string)
  default = {
    Name = "iics-secure-agent"
    tier = "test"
  }
}

variable "ecs_service_name" {
  default = "iics-agent-service"
}

variable "efs_tags" {
  type = map(string)
  default = {
    Name = "iics-secure-agent"
    tier = "test"
  }
}

variable "secure_agent_mount_path" {
  default = "/home/agent/infaagent/apps/agentcore/"
}

variable "secure_agent_sg_tags" {
  default = {
    Name = "secure-agent-efs-sg"
    tier = "test"
  }
}

variable "iam_instance_profile" {
  default = "secure-agent-ecs-instance-profile"
}

variable "secure_agent_efs_sg" {
  default = "secure-agent-efs-sg"
}

variable "iics_secret_access_policy" {
  default = "iics-secret-access-policy-test"
}

variable "ecs_optimized_ami" {
  default = "ami-00f69adbdc780866c"
}
