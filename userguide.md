## Table Of Contents
* [User Guide](#user-guide)
    * [Enterprise Integration Secure Agent](#enterprise-integration-secure-agent)
    * [Where To Get Support ?](#where-to-get-support-)

# User Guide 
[userguide]: #user-guide
This is the user guide for AWS hosted Secure Agent from Enterprise Integration team. This Secure Agent can be selected 
using the runtime names given in below table in IICS.

## Enterprise Integration Secure Agent
[eisecureagent]: #enterprise-integration-secure-agent
Secure Agent has following IP addresses. If any integration requires access to resources that are 
behind the cooperate firewalls(for example a database), a firewall exception for incoming traffic will require for 
following IP addresses. 

| environment| IP address |runtime name in IICS |
| -----------|-------------|-------------------------|
| test      | 3.230.240.5|ei.secureagent.doit.wisc.edu |
| prod      | 3.19.12.147|ei.secureagent.doit.wisc.edu|

## Where To Get Support ?
[support]: #where-to-get-support-
Enterprise Integration team can be reached at `ais-enterprise-integration-informatica@office365.wisc.edu` for any Secure Agent 
support.